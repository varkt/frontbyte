$(function () {
	// Preloader
    $(window).on('load', function () {
        let $preloader = $('#page-preloader'),
            $spinner   = $preloader.find('.cssload-loader');
        $spinner.fadeOut();
        $preloader.delay(350).fadeOut('slow');
    });

    // slow movement to the anchor
    $("a.js-menu__link").click(function (e) {
    	e.preventDefault();
    	elementClick = $(this).attr("href");
    	destination  = $(elementClick).offset().top;
    	$("body,html").animate({scrollTop: destination }, 1500);
    })
})